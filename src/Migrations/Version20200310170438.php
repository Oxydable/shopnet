<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200310170438 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item_commande DROP INDEX UNIQ_2E76EB6F347EFB, ADD INDEX IDX_2E76EB6F347EFB (produit_id)');
        $this->addSql('ALTER TABLE item_commande CHANGE produit_id produit_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE item_commande DROP INDEX IDX_2E76EB6F347EFB, ADD UNIQUE INDEX UNIQ_2E76EB6F347EFB (produit_id)');
        $this->addSql('ALTER TABLE item_commande CHANGE produit_id produit_id INT NOT NULL');
    }
}
