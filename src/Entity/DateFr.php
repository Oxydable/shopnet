<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Entité pour la date en français.
 * Class DateFr
 * @package App\Entity
 */
class DateFr
{
    /**
     * @var DateTime
     */
    private $datetime;

    /**
     * @var array
     */
    private $mois = [
        1 => "janvier",
        2 => "février",
        3 => "mars",
        4 => "avril",
        5 => "mai",
        6 => "juin",
        7 => "juillet",
        8 => "août",
        9 => "septembre",
        10 => "octobre",
        11 => "novembre",
        12 => "décembre"
    ];

    /**
     * @var array
     */
    private $semaines = [
        1 => "lundi",
        2 => "mardi",
        3 => "mercredi",
        4 => "jeudi",
        5 => "vendredi",
        6 => "samedi",
        7 => "dimanche"
    ];

    /**
     * DateFr constructor.
     * @param DateTime $dateTime
     */
    public function __construct(DateTime $dateTime)
    {
        $this->datetime = $dateTime;
    }


    /**
     * @return DateTime
     */
    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     */
    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return string
     */
    public function getAnnee()
    {
        return $this->datetime->format("Y");
    }

    /**
     * @return string
     */
    public function getAnneeNumerique()
    {
        return $this->datetime->format("y");
    }

    /**
     * @return mixed
     */
    public function getMois()
    {
        $index = $this->datetime->format("n");
        $index = intval($index);

        return $this->mois[$index];
    }

    /**
     * @return string
     */
    public function getMoisNumerique()
    {
        return $this->datetime->format("m");
    }

    /**
     * @return mixed
     */
    public function getSemaine()
    {
        $index = $this->datetime->format("N");
        $index = intval($index);

        return $this->semaines[$index];
    }

    /**
     * @return string
     */
    public function getJour()
    {
        return $this->datetime->format("j");
    }

    public function getHeure(){

        return $this->datetime->format("H");
    }

    public function getMinute(){

        return $this->datetime->format("i");
    }

    public function getSeconde(){

        return $this->datetime->format("s");
    }

    public function getTemps(){

        return $this->getHeure() . "h" . $this->getMinute();
    }


    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return
            $this->getJour() . " " .
            $this->getMois() . " " .
            $this->getAnnee()
            ;
    }
}
