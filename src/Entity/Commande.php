<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Entité pour une commande.
 * @ORM\Entity(repositoryClass="App\Repository\CommandeRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get"={"security"="is_granted('ROLE_USER')"},
 *          "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *     itemOperations={
 *          "get"={"security"="is_granted('ROLE_USER')"},
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *      normalizationContext={
 *          "groups"={"commande"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={"user": "exact", "items": "exact"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(RangeFilter::class, properties={"prix"})
 * @ApiFilter(OrderFilter::class, properties={"id", "date", "prix"}, arguments={"orderParameterName"="order"})
 */
class Commande
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"commande", "user"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commandes")
     * @Groups({"commande"})
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemCommande", mappedBy="commande")
     * @Groups({"commande", "user"})
     */
    private $items;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"commande", "user"})
     */
    private $date;
    private $dateFr;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Groups({"commande", "user"})
     */
    private $prix;


    public function __construct()
    {
        $this->date = new \DateTime("NOW");
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|ItemCommande[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function addItem(ItemCommande $item): self
    {
        if (!$this->items->contains($item)) {
            $this->items[] = $item;
            $item->setCommande($this);
        }

        return $this;
    }

    public function removeItem(ItemCommande $item): self
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
            // set the owning side to null (unless already changed)
            if ($item->getCommande() === $this) {
                $item->setCommande(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Commande n°" . $this->id;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateFr()
    {
        $this->dateFr = new DateFr($this->date);
        return $this->dateFr;
    }

    public function getPrix(): ?float
    {
        return $this->prix;
    }

    public function setPrix(?float $prix): self
    {
        $this->prix = $prix;

        return $this;
    }



}
