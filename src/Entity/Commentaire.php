<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Entité pour un commentaire.
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_USER')", "access_control_message"="Je suis un message"}
 *      },
 *     itemOperations={
 *          "get",
 *          "put"={"security"="is_granted('ROLE_USER')"},
 *          "patch"={"security"="is_granted('ROLE_USER')"},
 *          "delete"={"security"="is_granted('ROLE_USER')"}
 *      },
 *      normalizationContext={
 *          "groups"={"commentaire"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={"user": "exact", "message": "partial", "produit" : "exact"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(OrderFilter::class, properties={"id", "date"}, arguments={"orderParameterName"="order"})
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"commentaire"})
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Groups({"commentaire"})
     *
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commentaires")
     * @Groups({"commentaire"})
     *
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="commentaires")
     * @Groups({"commentaire"})
     *
     */
    private $produit;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"commentaire"})
     *
     */
    private $date;
    private $dateFr;

    /**
     * Commentaire constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->date = new \DateTime("NOW");
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $Message): self
    {
        $this->message = $Message;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateFr()
    {
        $this->dateFr = new DateFr($this->date);
        return $this->dateFr . " " . $this->dateFr->getTemps();
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Commentaire " . $this->id . " sur le " . $this->produit;
    }


}
