<?php

namespace App\Entity;

use App\Controller\FichierController;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Entité pour un fichier.
 * Class Fichier
 * @package App\Entity
 */
class Fichier
{
    private const USER_DEFAULT_IMAGE = "default-user-profil.svg";
    private const DIRECTORY_PRODUCT = "img/product";
    private const DIRECTORY_PROFIL = "img/profil";

    private $nom;

    private $chemin;

    /**
     * Fichier constructor.
     */
    public function __construct()
    {
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getDefaultUserPicture(){
        return self::DIRECTORY_PROFIL . '/' . self::USER_DEFAULT_IMAGE;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->chemin . $this->nom;
    }

    public function uploadProfilFile(UploadedFile $f)
    {

        if ($f) {
            $originalFilename = pathinfo($f->getClientOriginalName(), PATHINFO_FILENAME);

            // this is needed to safely include the file name as part of the URL
            $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
            $newFilename = $safeFilename.'-' . uniqid() . '.' . $f->guessExtension();

            // Move the file to the directory where brochures are stored
            try {
                $f->move(
                    self::DIRECTORY_PROFIL,
                    $newFilename
                );
            } catch (FileException $e) {
                // ... handle exception if something happens during file upload
                dd($e->getMessage());
            }

            $this->setNom($newFilename);
            $this->setChemin(self::DIRECTORY_PROFIL);
        }
    }

    public function deleteProfilFile( $chemin ){

        if(is_file($chemin) && $chemin !== $this->getDefaultUserPicture() ){
            unlink($chemin);
        }

    }

}
