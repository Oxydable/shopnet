<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use \DateTime;
use Exception;
use phpDocumentor\Reflection\DocBlock\Description;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Entité d'un produit.
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *     itemOperations={
 *          "get",
 *          "put"={"security"="is_granted('ROLE_ADMIN')"},
 *          "patch"={"security"="is_granted('ROLE_USER')"},
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *      normalizationContext={
 *          "groups"={"produit"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={
 *     "nom": "exact",
 *     "categorie" : "exact",
 *     "tags" : "exact",
 *     "commentaires" : "exact",
 *     "itemCommandes" : "exact",
 *     "description": "partial"
 * })
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(RangeFilter::class, properties={"prix", "quantite"})
 * @ApiFilter(OrderFilter::class,
 *     properties={"id", "nom", "categorie", "date", "prix", "quantite"},
 *     arguments={"orderParameterName"="order"}
 *)
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"produit", "categorie", "commentaire", "itemCommande"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=2255)
     * @Groups({"produit", "categorie"})
     */
    private $nom;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     * @Groups({"produit", "categorie"})
     */
    private $prix;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({ "produit", "categorie"})
     */
    private $date;
    private $dateFr;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({ "produit"})
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categorie", inversedBy="produits", cascade={"persist"})
     * @Groups({"produit"})
     */
    private $categorie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Image", mappedBy="produit")
     * @Groups({ "produit", "categorie"})
     */
    private $images;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", mappedBy="produits")
     * @Groups({ "produit"})
     */
    private $tags;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"produit", "itemCommande"})
     *
     */
    private $quantite;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="produit")
     *
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ItemCommande", mappedBy="produit")
     *
     */
    private $itemCommandes;

    /**
     * Produit constructor.
     * @param int $id
     * @param string $nom
     * @param float $prix
     * @param null $description
     * @throws Exception
     */
    public function __construct($id=0, $nom="ProductDefault", $prix=0.0, $description=NULL)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->prix = $prix;
        $this->date = new DateTime('NOW');
        $this->description = $description;
        $this->images = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->itemCommandes = new ArrayCollection();

    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getNom(): ?string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return $this
     */
    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrix(): ?string
    {
        return $this->prix;
    }

    /**
     * @param string $prix
     * @return $this
     */
    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return $this
     */
    public function setDate(\DateTime $date): self
    {
        $this->date =  $date;
        return $this;
    }

    public function getDateFr()
    {
        $this->dateFr = new DateFr($this->date);
        return $this->dateFr;
    }

    /**
     * @return string|null
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Produit n°" . $this->getId();
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Categorie|null
     */
    public function getCategorie(): ?Categorie
    {
        return $this->categorie;
    }

    /**
     * @param Categorie|null $categorie
     * @return $this
     */
    public function setCategorie(?Categorie $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * @return Collection|Image[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    /**
     * @param Image $image
     * @return $this
     */
    public function addImage(Image $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setProduit($this);
        }

        return $this;
    }

    /**
     * @param Image $image
     * @return $this
     */
    public function removeImage(Image $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
            // set the owning side to null (unless already changed)
            if ($image->getProduit() === $this) {
                $image->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->addProduit($this);
        }

        return $this;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            $tag->removeProduit($this);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    /**
     * @param int|null $quantite
     * @return $this
     */
    public function setQuantite(?int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setProduit($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getProduit() === $this) {
                $commentaire->setProduit(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ItemCommande[]
     */
    public function getItemCommandes(): Collection
    {
        return $this->itemCommandes;
    }

    public function addItemCommande(ItemCommande $itemCommande): self
    {
        if (!$this->itemCommandes->contains($itemCommande)) {
            $this->itemCommandes[] = $itemCommande;
            $itemCommande->setProduit($this);
        }

        return $this;
    }

    public function removeItemCommande(ItemCommande $itemCommande): self
    {
        if ($this->itemCommandes->contains($itemCommande)) {
            $this->itemCommandes->removeElement($itemCommande);
            // set the owning side to null (unless already changed)
            if ($itemCommande->getProduit() === $this) {
                $itemCommande->setProduit(null);
            }
        }

        return $this;
    }







}
