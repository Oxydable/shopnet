<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
/**
 * Entité pour un élément lié à une commande.
 * @ORM\Entity(repositoryClass="App\Repository\ItemCommandeRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *     itemOperations={
 *          "get",
 *          "put"={"security"="is_granted('ROLE_USER')"},
 *          "patch"={"security"="is_granted('ROLE_USER')"},
 *          "delete"={"security"="is_granted('ROLE_USER')"}
 *      },
 *      normalizationContext={
 *          "groups"={"itemCommande"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={"commande": "exact", "produit": "exact", "taille" : "exact", "couleur" : "exact"})
 * @ApiFilter(RangeFilter::class, properties={"nombre"})
 * @ApiFilter(OrderFilter::class, properties={"id", "nombre", "taille", "couleur"}, arguments={"orderParameterName"="order"})
 */
class ItemCommande
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"commande", "itemCommande"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"itemCommande"})
     */
    private $taille;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"itemCommande"})
     */
    private $couleur;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Commande", inversedBy="items", cascade={"persist"})
     * @Groups({"itemCommande"})
     */
    private $commande;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"itemCommande"})
     */
    private $nombre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="itemCommandes",cascade={"persist"})
     * @Groups({"itemCommande"})
     */
    private $produit;


    /**
     * ItemCommande constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    public function setProduit(Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getCouleur(): ?string
    {
        return $this->couleur;
    }

    public function setCouleur(string $couleur): self
    {
        $this->couleur = $couleur;

        return $this;
    }

    public function getCommande(): ?Commande
    {
        return $this->commande;
    }

    public function setCommande(?Commande $commande): self
    {
        $this->commande = $commande;

        return $this;
    }

    public function getNombre(): ?int
    {
        return $this->nombre;
    }

    public function setNombre(int $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return "Item n°" . $this->getId();
    }


}
