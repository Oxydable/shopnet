<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Symfony\Component\Serializer\Annotation\Groups;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Entité pour la carte bancaire.
 * @ORM\Entity(repositoryClass="App\Repository\CBRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *     itemOperations={
 *          "get",
 *          "put",
 *          "delete"={"security"="is_granted('ROLE_USER')"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={"user": "exact", "titulaire": "exact"})
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(OrderFilter::class, properties={"id", "date", "titulaire"}, arguments={"orderParameterName"="order"})
 */
class CB
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]{4}( [0-9]{4}){3}$/",
     *     message="Veuillez respecter le format de numéro de carte bleue (Ex: 0123 4567 8901 2345)"
     * )
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\/([0-9][0-9])$/",
     *     message="Veuillez respecter le format des dates d'expirations (Ex: 01/99)"
     * )
     * @Groups({"user"})
     */
    private $date;
    private $dateFr;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Regex(
     *     pattern="/^[0-9]{3}$/",
     *     message="Veuillez respecter le format d'un cryptogramme (Ex: 159)"
     * )
     *
     */
    private $cryptogramme;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="CB")
     */
    private $user;

    /**
     * @Groups({"book"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern="/^[A-Z]( *[A-Z]*)+$/",
     *     message="Veuillez respecter le format d'un titulaire en majuscule (Ex: NOM PRENOM)"
     * )
     * @Groups({"user"})
     */
    private $titulaire;

    /**
     * CB constructor.
     */
    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getDateFr()
    {
        return $this->date;
    }

    public function getCryptogramme(): ?string
    {
        return $this->cryptogramme;
    }

    public function setCryptogramme(string $cryptogramme): self
    {
        $this->cryptogramme = $cryptogramme;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString()
    {
        return "Carte bleu de " . $this->user;
    }

    public function getTitulaire(): ?string
    {
        return $this->titulaire;
    }

    public function setTitulaire(?string $titulaire): self
    {
        $this->titulaire = $titulaire;

        return $this;
    }


}
