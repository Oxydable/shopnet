<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Entité pour une image.
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *     itemOperations={
 *          "get",
 *          "put"={"security"="is_granted('ROLE_USER')"},
 *          "patch"={"security"="is_granted('ROLE_USER')"},
 *          "delete"={"security"="is_granted('ROLE_USER')"}
 *      }
 *)
 * @ApiFilter(SearchFilter::class, properties={"user": "exact", "produit": "exact", "chemin" : "partial", "alternative" : "partial"})
 * @ApiFilter(OrderFilter::class, properties={"id"}, arguments={"orderParameterName"="order"})
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"produit", "categorie"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"produit", "categorie"})
     */
    private $chemin;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Produit", inversedBy="Images")
     */
    private $produit;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alternative;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", mappedBy="image", cascade={"persist", "remove"})
     */
    private $user;

    /**
     * Image constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    /**
     * @param string $chemin
     * @return $this
     */
    public function setChemin(string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->chemin;
    }

    /**
     * @return Produit|null
     */
    public function getProduit(): ?Produit
    {
        return $this->produit;
    }

    /**
     * @param Produit|null $produit
     * @return $this
     */
    public function setProduit(?Produit $produit): self
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlternative(): ?string
    {
        return $this->alternative;
    }

    /**
     * @param string|null $alternative
     * @return $this
     */
    public function setAlternative(?string $alternative): self
    {
        $this->alternative = $alternative;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        // set (or unset) the owning side of the relation if necessary
        $newImage = null === $user ? null : $this;
        if ($user->getImage() !== $newImage) {
            $user->setImage($newImage);
        }

        return $this;
    }



}
