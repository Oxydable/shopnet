<?php

namespace App\DataFixtures;

use App\Entity\Categorie;
use App\Entity\Image;
use App\Entity\Produit;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Controller qui gère les fausses données du site.
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /**
     * @var
     */
    private $faker;

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create("fr_FR");

        $categories = $this->loadCategorie();
        $produits = $this->loadProduit();
        $imgs = $this->loadImage();
        $tags = $this->loadTag();

        for($i = 0; $i < $produits->count(); $i++){

            $produit = $produits->get($i);
            $indexCategorie = 0;

            try {
                $indexCategorie = random_int( 0, $categories->count() -1 );
            } catch (\Exception $e) {
                error_log($e->getMessage());
            }

            $produit->setCategorie( $categories->get($indexCategorie) );
            $categories->get($indexCategorie)->addProduit($produit);

            $produit->addImage($imgs->get($i));
            $imgs->get($i)->setProduit($produit);

            $nombreTags = random_int( 1, $tags->count() -1 );

            for($j = 0; $j < $nombreTags; $j++){
                $indexTag = random_int( 0, $tags->count() -1 );

                $produit->addTag($tags->get($indexTag));
                $tags->get($indexTag)->addProduit($produit);
            }
        }

        foreach($produits as $nombre => $produit){
            $manager->persist($produit);
        }
        foreach($categories as $nombre => $categorie){
            $manager->persist($categorie);
        }
        foreach($imgs as $nombre => $img){
            $manager->persist($img);
        }
        foreach($tags as $nombre => $tag){
            $manager->persist($tag);
        }

        $manager->flush();
    }

    /**
     * @param int $nombre
     * @return ArrayCollection
     */
    public function loadCategorie($nombre=4)
    {
        $categories = new ArrayCollection();

        for( $i =0; $i < $nombre; $i++ ){
            $categorie = new Categorie();
            $categorie->setNom( $this->faker->word()  );
            $categorie->setDescription( $this->faker->realText(50) );

            $categories->add($categorie);
        }

        return $categories;
    }

    /**
     * @param int $nombre
     * @return ArrayCollection
     * @throws \Exception
     */
    public function loadProduit($nombre=100)
    {
        $produits = new ArrayCollection();

        for( $i =0; $i < $nombre; $i++ ){
            $produit = new Produit();
            $produit->setNom( $this->faker->word()  );
            $produit->setPrix( $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 175)  );
            $produit->setDescription( $this->faker->realText() );
            $produit->setQuantite( $this->faker->numberBetween($min = 1, $max = 150) );

            $produits->add($produit);
        }

        return $produits;
    }

    /**
     * @param int $nombre
     * @return ArrayCollection
     */
    public function loadImage($nombre=100)
    {
        $imgs = new ArrayCollection();

        for( $i =0; $i < $nombre; $i++ ){
            $img = new Image();
            $img->setChemin( "https://via.placeholder.com/640x480"  );
            $img->setAlternative( $this->faker->realText(10) );

            $imgs->add($img);
        }

        return $imgs;
    }

    /**
     * @param int $nombre
     * @return ArrayCollection
     */
    public function loadTag($nombre=20)
    {
        $tags = new ArrayCollection();

        for( $i =0; $i < $nombre; $i++ ){
            $tag = new Tag();
            $tag->setNom( $this->faker->word() );


            $tags->add($tag);
        }

        return $tags;
    }
}
