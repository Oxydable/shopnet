<?php

namespace App\Controller;

use App\Entity\Adresse;
use App\Entity\Categorie;
use App\Entity\CB;
use App\Entity\Commande;
use App\Entity\DateFr;
use App\Entity\Produit;
use App\Form\AdresseType;
use App\Form\CBType;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Le controller qui gère les commandes.
 * Class CommandeController
 * @package App\Controller
 */
class CommandeController extends AbstractController
{
    /**
     * La route pour un formulaire qui demande l'adresse du client.
     * @Route("/commande/adresse", name="commande_adresse")
     * @param Request $request
     * @return Response
     */
    public function adresse(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();
        $adresse = $user->getAdresse();
        if( $adresse === NULL){
            $adresse = new Adresse();
        }
        $form = $this->createForm(AdresseType::class, $adresse);

        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid()  ){
            $manager = $this->getDoctrine()->getManager();
            $user->setAdresse($adresse);
            $manager->persist($adresse);
            $manager->flush();

            return $this->redirectToRoute("commande_adresse");
        }

        return $this->render('commande/adresse.html.twig', [
            "categories" => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * La route pour un formulaire qui demande les informations de paiement du client.
     * @Route("/commande/cb", name="commande_cb")
     * @param Request $request
     * @return Response
     */
    public function cb(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();
        $cb = new CB();

        $form = $this->createForm(CBType::class, $cb);

        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid()  ){
            $manager = $this->getDoctrine()->getManager();

            $cb->setNumero( password_hash($form->get("numero")->getData(), PASSWORD_DEFAULT ) );
            $cb->setCryptogramme( password_hash($form->get("cryptogramme")->getData(), PASSWORD_DEFAULT ) );
            $cb->setDate( $form->get("date")->getData()  );
            $cb->setTitulaire( $form->get("titulaire")->getData() );
            $cb->setUser($user);

            $manager->persist($cb);
            $manager->flush();

            return $this->redirectToRoute("commande_cb");
        }

        return $this->render('commande/cb.html.twig', [
            "categories" => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * La route qui demande confirmation au client.
     * @Route("/commande/confirmation/{id}", name="commande_confirmation")
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function confirmation(int $id, Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $cb = $this->getDoctrine()->getRepository(CB::class)->find($id);
        $session = $request->getSession();
        $panier = $session->get("panier", []);

        return $this->render('commande/confirmation.html.twig', [
            "categories" => $categories,
            "cb" => $cb,
            "panier" => $panier
        ]);
    }

    /**
     * La route qui insert une commande dans la base de données.
     * @Route("/commande", name="commande_commande")
     * @param EntityManagerInterface $manager
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function commande(EntityManagerInterface $manager ,Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();
        $commande = new Commande();
        $session = $request->getSession();
        $panier = $session->get("panier", []);

        $prixTotal = 0.0;
        foreach( $panier as $nombre => $item)
        {
            $produit = $this->getDoctrine()->getRepository(Produit::class)->find($item->getProduit());
            $prixTotal += $produit->getPrix();

            $item->setCommande($commande);
            $item->setProduit($produit);

            $manager->persist($item);
            $manager->flush();
        }

        $session->set("panier", []);

        $commande->setUser($user);
        $commande->setPrix($prixTotal);

        $manager->persist($commande);
        $manager->flush();

        return $this->redirectToRoute("user_panier");
    }


    /**
     * La route pour voir une commande.
     * @Route("/commande/{id}", name="commande_show")
     * @param $id
     * @return Response
     */
    public function show($id)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $commande = $this->getDoctrine()->getRepository(Commande::class)->find($id);


        return $this->render('commande/show.html.twig', [
            "categories" => $categories,
            "commande" => $commande
        ]);
    }
}
