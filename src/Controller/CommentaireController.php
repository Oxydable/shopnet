<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Produit;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Le controller qui gère les commantaires.
 * Class CommentaireController
 * @package App\Controller
 */
class CommentaireController extends AbstractController
{

    /**
     * Supprime un commentaire.
     * @Route("/commentaires/supprimer/{categorie}/{id}/{commentaire}", name="commentaire_delete")
     * @param $categorie
     * @param $id
     * @param $commentaire
     * @return RedirectResponse
     */
    public function delete($categorie, $id, $commentaire)
    {
        $manager = $this->getDoctrine()->getManager();
        $commentaire = $this->getDoctrine()->getRepository(Commentaire::class)->find( intval($commentaire) );

        $manager->remove($commentaire);
        $manager->flush();

        return $this->redirectToRoute("app_product", array('categorie' => $categorie, "id" => $id ));

    }
}
