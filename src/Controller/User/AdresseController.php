<?php

namespace App\Controller\User;

use App\Entity\Adresse;
use App\Entity\Categorie;
use App\Form\AdresseType;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller qui gère l'adresse du profile.
 * Class AdresseController
 * @package App\Controller\User
 */
class AdresseController extends AbstractController
{
    /**
     * Modifier l'adresse.
     * @Route("/profil/adresse/modification", name="user_adresse_edit")
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request)
    {

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();

        $adresse =  $user->getAdresse();

        if( $adresse === NULL){
            $adresse = new Adresse();
        }

        $form = $this->createForm(AdresseType::class, $adresse);
        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid() ){

            $manager = $this->getDoctrine()->getManager();

            $user->setAdresse($adresse);

            $manager->persist($adresse);

            $manager->flush();

            return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()]);
        }

        return $this->render('user/adresse/edit.html.twig', [
            "categories" => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * Supprimer l'adresse.
     * @Route("/profil/adresse/supprimer", name="user_adresse_delete")
     * @return Response
     */
    public function delete()
    {
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $adresse = $user->getAdresse();

        $manager->remove($adresse);
        $manager->flush();

        return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()] );
    }
}
