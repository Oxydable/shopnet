<?php

namespace App\Controller\User;

use App\Entity\Categorie;
use App\Entity\CB;
use App\Form\CBType;
use App\Repository\CategorieRepository;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller qui gère les informations de paiement du profile.
 * Class CBController
 * @package App\Controller\User
 */
class CBController extends AbstractController
{
    /**
     * Ajouter une carte bancaire.
     * @Route("/CB/ajouter", name="user_cb_add")
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param CategorieRepository $doctrineCategorie
     * @return Response
     * @throws \Exception
     */
    public function add(Request $request, EntityManagerInterface $manager ,CategorieRepository $doctrineCategorie)
    {
        $categories = $doctrineCategorie->findAll();
        $user = $this->getUser();
        $cb = new CB();
        $form = $this->createForm(CBType::class, $cb);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() ) {

            /** Doc PHP :
             * PASSWORD_DEFAULT : Cette constante est concue pour changer dans le temps, au fur et à mesure que
             * des algorithmes plus récents et plus forts sont ajoutés à PHP.
            */
            //DateTime::createFromFormat( "d/m/y H:i", "01/" . $form->get("date")->getData() . " 23:59" )

            $cb->setNumero( password_hash($form->get("numero")->getData(), PASSWORD_DEFAULT ) );
            $cb->setCryptogramme( password_hash($form->get("cryptogramme")->getData(), PASSWORD_DEFAULT ) );
            $cb->setDate( $form->get("date")->getData() );
            $cb->setTitulaire( $form->get("titulaire")->getData() );
            $cb->setUser($user);

            $manager->persist($cb);
            $manager->flush();

            return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()]);
        }

        return $this->render('user/cb/add.html.twig', [
            'categories' => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * Supprimer une carte bancaire
     * @Route("/CB/supprimer/{id}", name="user_cb_delete")
     * @param $id
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function delete($id, EntityManagerInterface $manager )
    {
        $user = $this->getUser();
        $cb = $this->getDoctrine()->getRepository(CB::class)->find($id);

        $manager->remove($cb);
        $manager->flush();

        return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()]);
    }
}
