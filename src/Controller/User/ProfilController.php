<?php

namespace App\Controller\User;

use App\Entity\Categorie;
use App\Entity\Fichier;
use App\Entity\Image;
use App\Form\FichierType;
use App\Form\InformationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller qui gère le profile.
 * Class ProfilController
 * @package App\Controller\User
 */
class ProfilController extends AbstractController
{
    /**
     * La route pour un formulaire qui permet de modifier les informations du profile.
     * @Route("/profil/modification", name="user_profil_edit")
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request)
    {

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();

        $form = $this->createForm(InformationType::class, $user);
        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid() ){

            $manager = $this->getDoctrine()->getManager();

            $manager->persist($user);

            $manager->flush();

            return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()]);
        }

        return $this->render('user/profil/edit.html.twig', [
            "categories" => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * La route pour un formulaire qui permet d'upload un fichier pour le profile.
     * @Route("/profil/image", name="user_profil_image")
     * @param Request $request
     * @return Response
     */
    public function image(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $form = $this->createForm(FichierType::class);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $manager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $image = $this->getDoctrine()->getRepository(Image::class)->find( $user->getImage() );
            $fichier = new Fichier();
            $fileData = $form->get('fichier')->getData();

            if( $user->getImage()->getChemin() != $fichier->getDefaultUserPicture() ){
                $fichier->deleteProfilFile( $user->getImage() );
            }

            $fichier->uploadProfilFile($fileData);

            $image->setChemin( $fichier->getChemin() . "/" . $fichier->getNom() );

            $manager->persist($image);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()] );
        }

        return $this->render('fichier/fichier.html.twig', [
            "categories" => $categories,
            'form' => $form->createView()
        ]);
    }


    /**
     * Remmet l'image de profile par défaut.
     * @Route("/profil/image/reset", name="user_profil_image_reset")
     * @param Request $request
     * @return Response
     */
    public function resetImage(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $image = $this->getDoctrine()->getRepository(Image::class)->find( $user->getImage() );
        $fichier = new Fichier();

        if( $user->getImage()->getChemin() != $fichier->getDefaultUserPicture() ){
            $fichier->deleteProfilFile( $user->getImage() );
        }

        $image->setChemin( $fichier->getDefaultUserPicture() );

        $manager->persist($image);
        $manager->persist($user);
        $manager->flush();

        return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()] );


    }


}
