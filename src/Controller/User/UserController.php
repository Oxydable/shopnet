<?php

namespace App\Controller\User;

use App\Entity\Adresse;
use App\Entity\Categorie;
use App\Entity\Fichier;
use App\Entity\Image;
use App\Entity\User;
use App\Form\AdresseType;
use App\Form\InformationType;
use App\Form\InscriptionType;
use App\Form\PasswordChangeType;
use Doctrine\Common\Annotations\Annotation;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Controller qui gère l'utilisateur.
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractController
{

    private const USER_DEFAULT_IMAGE = "img/profil/male-user-default.svg";

    /**
     * Le formulaire pour ce connecter.
     * @Route("/connexion", name="user_connexion")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();

        if( $user !== NULL){
            return $this->redirectToRoute("profil", [ "pseudo" => $user->getPseudo() ]);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/connexion.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            "categories" => $categories
        ]);
    }

    /**
     * Pour ce déconnecter.
     * @Route("/deconnexion", name="user_deconnexion")
     * @throws \Exception
     */
    public function logout()
    {


        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * Formulaire pour s'incrire.
     * @Route("/inscription", name="user_inscription")
     * @param Request $request
     * @param UserPasswordEncoderInterface $security
     * @return RedirectResponse|Response
     * @throws \Exception
     */
    public function registration(Request $request, UserPasswordEncoderInterface $security)
    {
        $user = $this->getUser();
        if( $user !== NULL){
            return $this->redirectToRoute("user_profil", [ "pseudo" => $user->getPseudo() ]);
        }

        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = new User();
        $fichier = new Fichier();


        $form = $this->createForm(InscriptionType::class, $user);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid())
        {
            $manager = $this->getDoctrine()->getManager();
            $image = new Image();
            $image->setChemin($fichier->getDefaultUserPicture());
            $image->setAlternative( "Image par défaut de l'utilisateur");

            $user->setPassword( $security->encodePassword($user, $user->getPassword()) );
            $user->setImage( $image );
            $user->setDate(new \DateTime("NOW"));

            $manager->persist($image);
            $manager->persist($user);

            $manager->flush();

            return $this->redirectToRoute("user_connexion");
        }

        return $this->render("user/inscription.html.twig", array(
            "categories" => $categories,
            "form" => $form->createView()
        ));

    }

    /**
     * Pour supprimer son compte.
     * @Route("/supprimer", name="user_supprimer")
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function erase(EntityManagerInterface $manager)
    {
        try {
            $this->getUser();
        } catch (\Exception $e){
            return $this->redirectToRoute("app_home");
        }

        $user = $this->getDoctrine()->getRepository(User::class)->find( $this->getUser() );
        $fichier = new Fichier();

        if( $user->getImage()->getChemin() != $fichier->getDefaultUserPicture() ){
            $fichier->deleteProfilFile( $user->getImage() );
        }

        foreach( $user->getCB() as $nombre => $cb){
            $manager->remove($cb);
        }

        foreach( $user->getCommentaires() as $nombre => $commentaire){
            $manager->remove($commentaire);
        }
        $manager->remove($user);

        $manager->flush();
        return $this->redirectToRoute("user_supprimer");
    }

    /**
     * Pour voir son profile.
     * @Route("/profil/{pseudo}", name="user_profil")
     * @param $pseudo
     * @return Response
     */
    public function profil($pseudo)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();

        if( $user->getPseudo() !== $pseudo ) {
            return $this->redirectToRoute("user_profil", ["pseudo" => $user->getPseudo()]);
        }

        return $this->render('user/profil.html.twig', [
            "categories" => $categories
        ]);
    }

    /**
     * Pour voir son panier.
     * @Route("/panier", name="user_panier")
     * @param Request $request
     * @return Response
     */
    public function panier(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $session = $request->getSession();
        $panier = $session->get("panier", []);



        return $this->render('user/panier.html.twig', [
            "panier" => $panier,
            "categories" => $categories
        ]);
    }

    /**
     * Pour changer son mot de passe.
     * @Route("/user/profil/modification/motdepasse", name="user_change_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $security
     * @return Response
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $security)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $user = $this->getUser();
        $form = $this->createForm(PasswordChangeType::class);

        $form->handleRequest($request);
        if( $form->isSubmitted() && $form->isValid() )
        {
            if( $security->isPasswordValid($user, $form->get("currentPassword")->getData()))
            {
                $manager = $this->getDoctrine()->getManager();
                $user->setPassword(   $security->encodePassword($user, $form->get("newPassword")->getData() ) );

                $manager->persist($user);
                $manager->flush();

                return $this->redirectToRoute("user_deconnexion");

            } else {


                return $this->render('user/password/change.html.twig', [
                    "form" => $form->createView(),
                    "categories" => $categories
                ]);
            }
        }

        return $this->render('user/password/change.html.twig', [
            "form" => $form->createView(),
            "categories" => $categories
        ]);

    }
}
