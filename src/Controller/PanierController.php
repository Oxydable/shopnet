<?php

namespace App\Controller;

use App\Entity\ItemCommande;
use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * La controller qui gère le panier.
 * Class PanierController
 * @package App\Controller
 */
class PanierController extends AbstractController
{

    /**
     * Supprime un produit du panier.
     * @Route("/panier/supprimer/{index}", name="panier_delete")
     * @param $index
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return RedirectResponse
     */
    public function delete($index, Request $request, EntityManagerInterface $manager)
    {
        $session = $request->getSession();
        $panier = $session->get("panier", []);
        $item = $panier[$index];
        $produit =  $this->getDoctrine()->getRepository(Produit::class)->find( $item->getProduit() );

        $produit->setQuantite($produit->getQuantite() + $item->getNombre());

        unset($panier[$index]);

        $session->set("panier", $panier);

        $manager->persist($produit);
        $manager->flush();

        return $this->redirectToRoute("user_panier");
    }
}
