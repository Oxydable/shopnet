<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Commentaire;
use App\Entity\DateFr;
use App\Entity\ItemCommande;
use App\Entity\Produit;
use App\Form\AjoutPanierType;
use App\Form\CommentType;
use App\Form\ContactType;
use App\Repository\ProduitRepository;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Le controller principale de l'application.
 * Class AppController
 * @package App\Controller
 */
class AppController extends AbstractController
{
    /**
     * La route par défaut. Elle redirige vers la page d'accueil.
     * @Route("/", name="app_default")
     */
    public function index()
    {
        return $this->redirectToRoute("app_home");
    }

    /**
     * La page d'accueil.
     * @Route("/home", name="app_home")
     */
    public function home()
    {
        $produits = $this->getDoctrine()->getRepository(Produit::class)->findRandom(4);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $dateFr = new DateFr(new \DateTime("NOW"));

        return $this->render('app/index.html.twig', [
            "produits" => $produits,
            "categories" => $categories,
            "dateFr" => $dateFr
        ]);
    }

    /**
     * La route pour voir tout les produits.
     * @Route("/catégories", name="app_categories")
     * @return Response
     */
    public function categories()
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        return $this->render('app/categories.html.twig', [
            "categories" => $categories,
        ]);
    }

    /**
     * La route pour voir tout les produits d'une catégorie.
     * @Route("/catégories/{nom}", name="app_categorie")
     * @param $nom
     * @return Response
     */
    public function categorie($nom)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $categorie = $this->getDoctrine()->getRepository(Categorie::class)->findBy( array("nom" => $nom) )[0];

        return $this->render('app/categorie.html.twig', [
            "categories" => $categories,
            "categorie" => $categorie
        ]);
    }

    /**
     * La route pour voir un produit
     * @Route("/catégories/{categorie}/{id}", name="app_product")
     * @param $categorie
     * @param $id
     * @param Request $request
     * @param ProduitRepository $produitRepository
     * @return Response
     * @throws \Exception
     */
    public function product($categorie, $id, Request $request, ProduitRepository $produitRepository)
    {
        $produit = $produitRepository->find($id);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        //Formulaire Ajout Panier
        $formPanier = $this->createForm(ajoutPanierType::class);
        $formPanier->handleRequest($request);
        if( $formPanier->isSubmitted() && $formPanier->isValid() ) {
            $item = $this->addPanier($id, $formPanier->getData(), $request, $produitRepository );
            $manager = $this->getDoctrine()->getManager();

            $produit->setQuantite( $produit->getQuantite() - $item->getNombre());

            $manager->persist($produit);

            $manager->flush();

            return $this->redirectToRoute("app_product", ["categorie" => $produit->getCategorie(), "id" => $id] );
        }

        //Formulaire Ajout Commentaire
        $commentaire = new Commentaire();
        $formCommentaire = $this->createForm(CommentType::class, $commentaire);
        $formCommentaire->handleRequest($request);
        if( $formCommentaire->isSubmitted() && $formCommentaire->isValid() ) {

            $manager = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $produit = $this->getDoctrine()->getRepository(Produit::class)->find($id);

            $commentaire->setUser( $user );
            $commentaire->setProduit( $produit );

            $manager->persist($commentaire);
            $manager->flush();

            return $this->redirectToRoute("app_product", array( 'categorie' => $categorie, "id" => $id ));

        }

        return $this->render('app/product.html.twig', [
            'categorie' => $categorie,
            'produit' => $produit,
            'categories' => $categories,
            "formPanier" => $formPanier->createView(),
            "formCommentaire" => $formCommentaire->createView()
        ]);
    }

    /**
     * Ajouter un produit dans le panier.
     * @param $id
     * @param $data
     * @param Request $request
     * @param ProduitRepository $produitRepository
     * @return ItemCommande
     */
    public function addPanier($id, $data, Request $request, ProduitRepository $produitRepository)
    {
        $session = $request->getSession();
        $produit = $produitRepository->find($id);

        $panier = $session->get("panier", []);

        $item = new ItemCommande();
        $item->setProduit( $produit );
        $item->setCouleur( $data["couleurs"] );
        $item->setTaille( $data["tailles"] );
        $item->setNombre( $data["quantite"] );


        $i = 0;
        $inPanier = false;

        while( $i < count($panier) && !$inPanier){
            if ( $panier[$i]->getProduit()->getNom() == $item->getProduit()->getNom() && $panier[$i]->getTaille() == $item->getTaille() && $panier[$i]->getCouleur() == $item->getCouleur() ){
                $inPanier = true;
            } else{
                $i++;
            }

        }

        if( $inPanier ){
            $panier[$i]->setNombre( $panier[$i]->getNombre() + $item->getNombre() );
        } else {
            $panier[] = $item;
        }


        $session->set("panier", $panier);

        return $item;

    }

    /**
     * La route pour me contacter.
     * @Route("/contact", name="app_contact")
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @return Response
     */
    public function contact(Request $request, Swift_Mailer $mailer)
    {

        $form = $this->createForm(ContactType::class);
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->sendMail($form->getData(), $mailer);
            return $this->redirectToRoute("app_contact");
        }

        return $this->render('app/contact.html.twig', [
            'categories' => $categories,
            "form" => $form->createView()
        ]);
    }

    /**
     * M'envoyer un mail.
     * @param $data
     * @param Swift_Mailer $mailer
     */
    private function sendMail($data, Swift_Mailer $mailer)
    {
        $destination = "ma.portet@orange.fr";

        $message = (new Swift_Message($data["Sujet"]))
            ->setFrom($data["Adresse"])
            ->setTo($destination)
            ->setBody($data["Message"])
        ;

        $mailer->send($message);
    }
}
