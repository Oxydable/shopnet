<?php

namespace App\Controller;

use App\Entity\DateFr;
use App\Entity\Fichier;
use App\Entity\Image;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Controller qui gère l'api.
 * Class MyApiController
 * @package App\Controller
 */
class MyApiController extends AbstractController
{
    /**
     * La route qui permet d'encoder un mot de passe.
     * @Route("/api/encode/password", name="api_encode_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $security
     * @return JsonResponse
     */
    public function encodePassword(Request $request, UserPasswordEncoderInterface $security)
    {
        //Juste pour l'interface de encodePassword
        $fakeUser = new User();
        $data = json_decode($request->getContent(), true);
        $data["password"] = $security->encodePassword($fakeUser, $data["password"]);
        return $this->json($data);
    }

    /**
     * La route qui permet d'avoir la date actuel en français.
     * @Route("/api/date/now", name="api_date_now")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function dateNow(Request $request)
    {
        $date = new DateFr(new \DateTime("NOW", new \DateTimeZone("Europe/Paris")));
        return $this->json($date);
    }

    /**
     * La route qui permet d'avoir la date en français.
     * @Route("/api/date/fr", name="api_date_fr")
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function dateFormatFr(Request $request)
    {
        $dateRequest = json_decode( $request->getContent(), true );
        $datefr = new DateFr(new \DateTime($dateRequest["date"], new \DateTimeZone("Europe/Paris")));
        return $this->json($datefr);
    }


    /**
     * La route qui permet de créer un nouvel utilisateur.
     * @Route("/api/new/user", name="api_new_user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $security
     * @return Response
     * @throws \Exception
     */
    public function newUser(Request $request, UserPasswordEncoderInterface $security)
    {
        try {

            $data = json_decode($request->getContent(), true);

            $user = new User();
            $user->setNom( $data["nom"] );
            $user->setPrenom( $data["prenom"] );
            $user->setPseudo( $data["pseudo"] );
            $user->setEmail( $data["email"] );
            $user->setPassword( $security->encodePassword( $user, $data["password"] ) );

            $fichier = new Fichier();
            $image = new Image();
            $image->setChemin( $fichier->getDefaultUserPicture() );
            $image->setAlternative( "Image par défaut de l'utilisateur");

            $user->setImage( $image );
            $user->setDate(new \DateTime("NOW", new \DateTimeZone("Europe/Paris")));

            $manager = $this->getDoctrine()->getManager();

            $manager->persist($image);
            $manager->persist($user);

            $manager->flush();

            return new JsonResponse([
                "message" => "New user saved",
                "succes" => true,
                "errorMessage" => null,
                "errorCode" => null,
                "errorLine" => null
            ]);

        } catch (\Exception $e){
            return new JsonResponse([
                "message" => "New user not saved",
                "succes" => false,
                "errorMessage" => $e->getMessage(),
                "errorCode" => $e->getCode(),
                "errorLine" => $e->getLine()
            ]);
        }
    }
}
