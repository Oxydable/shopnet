<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Fichier;
use App\Form\FichierType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Vich\UploaderBundle\Handler\DownloadHandler;

class FichierController extends AbstractController
{
    /**
     * Permet d'upload un fichier.
     * @Route("/fichier", name="fichier")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function uploadFichier(Request $request)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();

        $form = $this->createForm(FichierType::class);
        $form->handleRequest($request);

        if( $form->isSubmitted() && $form->isValid() )
        {
            $manager = $this->getDoctrine()->getManager();
            $fichier = new Fichier();
            $file = $form->get('fichier')->getData();

            $fichier->uploadProfilFile($file);

            $manager->persist($fichier);
            $manager->flush();

            return $this->redirectToRoute("fichier");
        }

        return $this->render('fichier/fichier.html.twig', [
            "categories" => $categories,
            'form' => $form->createView()
        ]);
    }
}
