<?php

namespace App\Controller;

use App\Entity\Categorie;
use App\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller qui gère les tags.
 * Class TagController
 * @package App\Controller
 */
class TagController extends AbstractController
{

    /**
     * Recherche tout les produits qui ont un certain 'tag'.
     * @Route("/search/tags/{tag}", name="tag_search")
     * @param $tag
     * @return Response
     */
    public function search($tag)
    {
        $categories = $this->getDoctrine()->getRepository(Categorie::class)->findAll();
        $tag = $this->getDoctrine()->getRepository(Tag::class)->findBy(["nom" => $tag])[0];

        return $this->render('tag/search.html.twig', [
            "categories" => $categories,
            "tag" => $tag
        ]);
    }
}
