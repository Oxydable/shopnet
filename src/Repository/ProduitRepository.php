<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    /**
     * ProduitRepository constructor.
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    /**
     * @param $categorie
     * @return mixed
     */
    public function findByCategorie($categorie)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param $tag
     * @return mixed
     */
    public function findByTag($tag)
    {
        return $this->createQueryBuilder('p')
            ->join("p.tags", "t", "WITH", "t.id = :tag")
            ->setParameter("tag", $tag)
            ->where("p.id = t.id")
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * @param $value
     * @return mixed
     */
    public function findRandom($value)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('p')
            ->setMaxResults($value)
        ;

        return $qb->getQuery()->getResult();
    }
}
