<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

/**
 * Formulaire pour change son mot de passe.
 * Class PasswordChangeType
 * @package App\Form
 */
class PasswordChangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('currentPassword', PasswordType::class, [
                "label" => "Mot de passe courant"
            ])
            ->add("newPassword", PasswordType::class, [
                "label" => "Nouveau mot de passe",
                "constraints" => [
                    new Constraints\Length([
                        "min" => 8,
                        "max" => 255,
                        "minMessage" => "Votre mot de passe doit faire une longueur de minumum {{ limit }} caractères." ,
                        "maxMessage" => "La longueur maximal de {{ limit }} caractères est atteint.",
                    ])
                ]
            ])
            ->add("confirmer", SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
