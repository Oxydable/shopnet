<?php

namespace App\Form;

use App\Entity\CB;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour une carte bancaire.
 * Class CBType
 * @package App\Form
 */
class CBType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero', TextType::class, [
                "label" => "Numéro de carte"
            ])
            ->add('date', TextType::class, [
                "label" => "Date d'expiration"
            ])
            ->add('cryptogramme', TextType::class, [
                "label" => "Cryptogramme"
            ])
            ->add('titulaire', TextType::class, [
                "label" => "Titulaire"
            ])
            ->add("confirmer", SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
