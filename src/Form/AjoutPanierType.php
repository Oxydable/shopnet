<?php

namespace App\Form;


use App\Entity\Produit;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

/**
 * Formulaire pour ajouter un produit au panier.
 * Class AjoutPanierType
 * @package App\Form
 */
class AjoutPanierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tailles', ChoiceType::class,[
                "label" => "Taille",
                "choices" => [
                    "Petit(S)" => "S",
                    "Moyen(M)" => "M",
                    "Large(L)" => "L",
                    "Très Large(XL)" => "XL"
                ],
                "constraints" => [
                    new Constraints\Choice([
                        "choices" => ["S", "M", "L", "XL"],
                        "message" => "Veuillez choisir une taille valide."
                    ])
                ]
            ])
            ->add('couleurs', ChoiceType::class,[
                "label" => "Couleur",
                "choices" => [
                    "Blanc" => "blanc",
                    "Noir" => "noir",
                    "Bleu" => "bleu",
                    "Rose" => "rose"
                ],
                "constraints" => [
                    new Constraints\Choice([
                        "choices" => ["blanc", "noir", "bleu", "rose"],
                        "message" => "Veuillez choisir une couleur valide."
                    ])
                ]
            ])
            ->add('quantite', TextType::class, array(
                "label" => "Quantité",
                "constraints" => array( new Constraints\Positive(array(
                    "message" => "Il faut que le nombre soit positive."
                ))
                )
            ))
            ->add('ajouter_au_panier', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
