<?php

namespace App\Form;

use App\Entity\Adresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Formulaire pour une adresse.
 * Class AdresseType
 * @package App\Form
 */
class AdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ville',TextType::class, [
                "label" => "Ville"
            ])
            ->add('codePostal', TextType::class,[
                "label" => "Code Postal"
            ])
            ->add('telephone', TextType::class, [
                    "label" => "Téléphone"
            ])
            ->add('rue', TextType::class, [
                "label" => "Rue"
            ])
            ->add('information', TextareaType::class, [
                "label" => "Informations Supplémentaires"
            ])
            ->add('confirmer', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adresse::class,
        ]);
    }
}
